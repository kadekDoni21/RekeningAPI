﻿using CoreApiResponse;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RekeningBank.Api.Data;
using RekeningBank.Api.Data.Model;
using RekeningBank.Api.Model;

namespace RekeningBank.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsuranceController : BaseController
    {
        private readonly RekeningBankDbContext dbContext;

        public InsuranceController(RekeningBankDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet("get-customer-insurance/{id}")]
        public IActionResult GetCustomerInsurance(Guid id)
        {
            List<CustomerInsuranceResponse> result = new List<CustomerInsuranceResponse>();
            List<CustomerInsurance> listCustomerInsurance = dbContext.customerInsurances.Where(x => x.SavingsAccountID.Equals(id)).ToList();
            if (listCustomerInsurance.Count() == 0)
            {
                return CustomResult("Not Found", System.Net.HttpStatusCode.NotFound);
            }

            foreach(var data in listCustomerInsurance)
            {
                Insurance insurance = dbContext.insurances.Find(data.InsuranceID);

                CustomerInsuranceResponse customerInsuranceResponse = new CustomerInsuranceResponse();
                customerInsuranceResponse.CustomerInsurance = data;
                customerInsuranceResponse.Insurance = insurance;
                result.Add(customerInsuranceResponse);
            }

            return CustomResult("Seccess", result, System.Net.HttpStatusCode.OK);
        }

        [HttpGet("get-customer-insurance-by-id/{id}")]
        public IActionResult GetCustomerInsuranceById(Guid id)
        {
            var customerInsurance = dbContext.customerInsurances.Find(id);
            if (customerInsurance == null)
            {
                return CustomResult("Not Found", System.Net.HttpStatusCode.NotFound);
            }

            var insurance = dbContext.insurances.Find(customerInsurance.InsuranceID);
            if (insurance == null)
            {
                return CustomResult("Not Found", System.Net.HttpStatusCode.NotFound);
            }

            var result = new DetailCustomerInsurance
            {
                ID = customerInsurance.ID,
                Name = insurance.Name,
                Description = insurance.Description,
                Cost = insurance.Cost,
                IsActive = customerInsurance.IsActive,
                CreatedDate = customerInsurance.CreatedDate
            };

            return CustomResult("Success", result, System.Net.HttpStatusCode.OK);
        }

        [HttpGet("get-insurance")]
        public IActionResult GetInsurance()
        {
            var result = dbContext.insurances.ToList();
            if (result.Count == 0)
            {
                return CustomResult("Not Found", System.Net.HttpStatusCode.Forbidden);
            }

            return CustomResult("Success", result, System.Net.HttpStatusCode.OK);
        }

        [HttpPost("add-customer-insurance")]
        public IActionResult addCustomerInsurance([FromBody] AddCustomerInsuranceRequest request)
        {
            foreach(var insuranceID in request.InsurancesID)
            {
                CustomerInsurance customerInsurance = new CustomerInsurance
                {
                    ID = Guid.NewGuid(),
                    InsuranceID = Guid.Parse(insuranceID),
                    SavingsAccountID = request.customerID,
                    IsActive = true,
                    CreatedBy = "insurance_add-custoemr-insurance",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                };

                dbContext.customerInsurances.Add(customerInsurance);
                dbContext.SaveChanges();
            }

            var listCustomerInsurance = dbContext.customerInsurances.Where(x => x.SavingsAccountID.Equals(request.customerID)).ToList();

            return CustomResult("Success", listCustomerInsurance, System.Net.HttpStatusCode.OK);
        }
    }
}
