﻿using Microsoft.AspNetCore.Mvc;
using RekeningBank.Api.Data;
using CoreApiResponse;
using RekeningBank.Api.Data.Model;
using System.Net;
using RekeningBank.Api.Model;
using Newtonsoft.Json;
using System.Text;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RekeningBank.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SavingsController : BaseController
    {
        private readonly RekeningBankDbContext dbContext;

        public SavingsController(RekeningBankDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet("get-savings-account/{id}")]
        public IActionResult GetSavingsAccount(Guid id)
        {
            var savingAccount = dbContext.savingsAccounts.Where(x=>x.ID.Equals(id)).FirstOrDefault();
            if (savingAccount == null)
            {
                return CustomResult("Not Found", System.Net.HttpStatusCode.NotFound);
            }

            var savingType = dbContext.savingTypes.Find(savingAccount.SavingsTypeId);
            SavingsAccountResponse savingsAccountResponse = new SavingsAccountResponse
            {
                SavingsAccount = savingAccount,
                SavingType = savingType
            };

            return CustomResult("Success", savingsAccountResponse, System.Net.HttpStatusCode.OK);
        }

        [HttpGet("get-savings-type")]
        public IActionResult GetSavingType()
        {
            var savingType = dbContext.savingTypes.ToList();

            return CustomResult("Success", savingType,System.Net.HttpStatusCode.OK);
        }

        [HttpPost("add-savings-account")]
        public IActionResult AddSavingsAccount([FromBody] SavingsAccountRequest request)
        {
            SavingsAccount savingsAccount = new SavingsAccount
            {
                ID = new Guid(),
                SavingsTypeId = request.SavingsTypeId,
                RekeningNumber = request.RekeningNumber,
                Pin = request.Pin,
                Balance = request.Balance
            };
            dbContext.savingsAccounts.Add(savingsAccount);
            dbContext.SaveChanges();

            //hit api customer for update data customer.savingaccountID

            UpdateCustomerSavingsAccountIdModel updateCustomerSavingsAccountIdModel = new UpdateCustomerSavingsAccountIdModel
            {
                CustomerID = request.CustomerID,
                SavingsAccountID = savingsAccount.ID
            };

            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(updateCustomerSavingsAccountIdModel);
            var requestAPI = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5213/api/Customer/add-saving-account");
            requestAPI.Content = new StringContent(json, Encoding.UTF8);
            requestAPI.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var response = client.Send(requestAPI);
            var content = response.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    return CustomResult("Success", savingsAccount, System.Net.HttpStatusCode.OK);
                }
                else
                {
                    return CustomResult("Failed", System.Net.HttpStatusCode.InternalServerError);
                }
            }

            return CustomResult("Success", savingsAccount, System.Net.HttpStatusCode.OK);
        }

        [HttpPost("delete-savings-account")]
        public IActionResult deleteSavingAccount([FromBody] UpdateCustomerSavingsAccountIdModel request)
        {
            var savingsAccount = dbContext.savingsAccounts.Find(request.SavingsAccountID);
            if(savingsAccount == null)
            {
                return CustomResult("Failed", System.Net.HttpStatusCode.NotFound);
            }
            dbContext.savingsAccounts.Remove(savingsAccount);
            dbContext.SaveChanges();

            var client = new HttpClient();
            request.SavingsAccountID = new Guid();
            var json = JsonConvert.SerializeObject(request);
            var requestAPI = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5213/api/Customer/add-saving-account");
            requestAPI.Content = new StringContent(json, Encoding.UTF8);
            requestAPI.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var response = client.Send(requestAPI);
            var content = response.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    return CustomResult("Success", savingsAccount, System.Net.HttpStatusCode.OK);
                }
                else
                {
                    return CustomResult("Failed", System.Net.HttpStatusCode.InternalServerError);
                }
            }

            return CustomResult("success", System.Net.HttpStatusCode.OK);
        }
    }
}
