﻿namespace RekeningBank.Api.Model
{
    public class UpdateCustomerSavingsAccountIdModel
    {
        public Guid CustomerID { get; set; }
        public Guid SavingsAccountID { get; set; }
    }
}
