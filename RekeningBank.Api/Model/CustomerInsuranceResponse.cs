﻿using RekeningBank.Api.Data.Model;

namespace RekeningBank.Api.Model
{
    public class CustomerInsuranceResponse
    {
        public CustomerInsurance CustomerInsurance { get; set; }
        public Insurance Insurance { get; set; }
    }
}
