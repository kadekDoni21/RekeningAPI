﻿namespace RekeningBank.Api.Model
{
    public class ApiResultModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}
