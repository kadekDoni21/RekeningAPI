﻿namespace RekeningBank.Api.Data.Model
{
    public class SavingType
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public int MinimumIntialDeposit { get; set; }
        public int AdministrativeCost { get;}
        public int MinimalWithDrawl { get; set; }
    }
}
