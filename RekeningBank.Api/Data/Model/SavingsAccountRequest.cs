﻿namespace RekeningBank.Api.Data.Model
{
    public class SavingsAccountRequest
    {
        public Guid CustomerID { get; set; }
        public Guid ID { get; set; }
        public Guid SavingsTypeId { get; set; }
        public string RekeningNumber { get; set; }
        public int Pin { get; set; }
        public long Balance { get; set; }
        public SavingType? SavingsType { get; set; }
    }
}
