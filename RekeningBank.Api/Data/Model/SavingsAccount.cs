﻿namespace RekeningBank.Api.Data.Model
{
    public class SavingsAccount
    {
        public Guid ID { get; set; }
        public Guid SavingsTypeId { get; set; }
        public string RekeningNumber { get; set; }
        public int Pin { get; set; }
        public long Balance { get; set; }

        //Navigation Properties
        public SavingType SavingsType { get; set; }
    }
}
