﻿namespace RekeningBank.Api.Data.Model
{
    public class Insurance
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description{ get; set; }
        public int Cost { get; set; }
    }
}
