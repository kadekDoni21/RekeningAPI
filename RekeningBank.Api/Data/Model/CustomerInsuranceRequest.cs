﻿namespace RekeningBank.Api.Data.Model
{
    public class CustomerInsuranceRequest
    {
        public Guid SavingsAccountID { get; set; }
        public Guid InsuranceID { get; set; }
    }
}
