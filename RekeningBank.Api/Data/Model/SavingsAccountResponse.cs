﻿namespace RekeningBank.Api.Data.Model
{
    public class SavingsAccountResponse
    {
        public SavingsAccount SavingsAccount { get; set; }
        public SavingType SavingType { get; set; }
    }
}
