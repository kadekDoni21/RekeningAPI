﻿using Microsoft.EntityFrameworkCore;
using RekeningBank.Api.Data.Model;

namespace RekeningBank.Api.Data
{
    public class RekeningBankDbContext : DbContext
    {
        public RekeningBankDbContext(DbContextOptions dbContextOptions): base(dbContextOptions)
        {
            
        }

        public DbSet<Insurance> insurances { get; set; }
        public DbSet<SavingType> savingTypes { get; set; }
        public DbSet<SavingsAccount> savingsAccounts { get; set; }
        public DbSet<CustomerInsurance> customerInsurances { get; set; }
    }
}
