﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RekeningBank.Api.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "insurances",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Cost = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_insurances", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "savingTypes",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MinimumIntialDeposit = table.Column<int>(type: "int", nullable: false),
                    MinimalWithDrawl = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_savingTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "customerInsurances",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SavingsAccountID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InsuranceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedDate = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customerInsurances", x => x.ID);
                    table.ForeignKey(
                        name: "FK_customerInsurances_insurances_InsuranceID",
                        column: x => x.InsuranceID,
                        principalTable: "insurances",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "savingsAccounts",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SavingsTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RekeningNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Pin = table.Column<int>(type: "int", nullable: false),
                    Balance = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_savingsAccounts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_savingsAccounts_savingTypes_SavingsTypeId",
                        column: x => x.SavingsTypeId,
                        principalTable: "savingTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_customerInsurances_InsuranceID",
                table: "customerInsurances",
                column: "InsuranceID");

            migrationBuilder.CreateIndex(
                name: "IX_savingsAccounts_SavingsTypeId",
                table: "savingsAccounts",
                column: "SavingsTypeId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "customerInsurances");

            migrationBuilder.DropTable(
                name: "savingsAccounts");

            migrationBuilder.DropTable(
                name: "insurances");

            migrationBuilder.DropTable(
                name: "savingTypes");
        }
    }
}
